package browser

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {

	var epiphany = Browser{"epiphany", []string{"/path/to/epiphany"}}
	var chromium = Browser{"chromium", []string{"/path/to/chromium"}}
	var firefox = Browser{"firefox", []string{"/path/to/firefox"}}
	var bconf = BrowsersConfig{
		Fallback:  "firefox",
		Available: []Browser{epiphany, chromium, firefox},
	}

	type args struct {
		bconf BrowsersConfig
		name  string
	}

	tests := []struct {
		name string
		args args
		want Browser
	}{
		{"Has a match", args{bconf, "epiphany"}, epiphany},
		{"Hasn't got a match", args{bconf, "test"}, firefox},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args.bconf, tt.args.name); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}

}
