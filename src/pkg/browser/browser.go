package browser

type BrowsersConfig struct {
	Fallback  string
	Available []Browser
}

type Browser struct {
	Name    string
	Command []string
}

func New(bconf BrowsersConfig, name string) Browser {
	for _, browser := range bconf.Available {
		if browser.Name == name {
			return browser
		}
	}
	return New(bconf, bconf.Fallback)
}
