package link

import (
	"fmt"
	"os"
	"regexp"
	"syscall"

	"gitlab.com/danielquinn/opening/pkg/browser"
	"gitlab.com/danielquinn/opening/pkg/config"
)

type Link struct {
	Config *config.Config
	Url    string
}

// Find the appropriate browser using the `URL` in the struct, and return the
// command for it.
func (l *Link) getBrowserCommand() []string {
	for _, url := range l.Config.Urls {
		regex := regexp.MustCompile(url.Regex)
		if regex.FindStringIndex(l.Url) != nil {
			return browser.New(l.Config.Browsers, url.Browser).Command
		}
	}
	return browser.New(l.Config.Browsers, l.Config.Browsers.Fallback).Command
}

func (l *Link) Open() error {

	executable := l.getBrowserCommand()

	var env []string
	env = append(env, os.Environ()[:]...)

	if err := syscall.Exec(executable[0], append(executable, l.Url), env); err != nil {
		return fmt.Errorf("couldn't run %s: %w", executable, err)
	}

	return nil

}
