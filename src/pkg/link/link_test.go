package link

import (
	"reflect"
	"testing"

	"gitlab.com/danielquinn/opening/pkg/browser"
	"gitlab.com/danielquinn/opening/pkg/config"
	"gitlab.com/danielquinn/opening/pkg/url"
)

func TestLink_getBrowserCommand(t *testing.T) {

	conf := &config.Config{
		Browsers: browser.BrowsersConfig{
			Fallback: "firefox",
			Available: []browser.Browser{
				{Name: "epiphany", Command: []string{"/path/to/epiphany"}},
				{Name: "chromium", Command: []string{"/path/to/chromium"}},
				{Name: "firefox", Command: []string{"/path/to/firefox"}},
			},
		},
		Urls: []url.Url{
			{Regex: `^https://test.ca/0`, Browser: "chromium"},
			{Regex: `^https://test.ca/1`, Browser: "epiphany"},
			{Regex: `^https://test.ca/2`, Browser: "firefox"},
		},
	}

	type fields struct {
		Config *config.Config
		Url    string
	}

	tests := []struct {
		name   string
		fields fields
		want   []string
	}{
		{
			name:   "URL matches",
			fields: fields{Config: conf, Url: "https://test.ca/1"},
			want:   []string{"/path/to/epiphany"},
		},
		{
			name:   "URL matches when the default is the same",
			fields: fields{Config: conf, Url: "https://test.ca/2"},
			want:   []string{"/path/to/firefox"},
		},
		{
			name:   "No match",
			fields: fields{Config: conf, Url: "https://test.nl/"},
			want:   []string{"/path/to/firefox"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &Link{Config: tt.fields.Config, Url: tt.fields.Url}
			if got := l.getBrowserCommand(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getBrowserCommand() = %v, want %v", got, tt.want)
			}
		})
	}

}
