package config

import (
	"gitlab.com/danielquinn/opening/pkg/browser"
	"gitlab.com/danielquinn/opening/pkg/url"
)

type Config struct {
	Browsers browser.BrowsersConfig
	Urls     []url.Url
}
