package main

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/urfave/cli/v2"
	"log"
	"os"

	"gitlab.com/danielquinn/opening/pkg/config"
	"gitlab.com/danielquinn/opening/pkg/link"
)

func main() {
	app := &cli.App{
		Action: func(cCtx *cli.Context) error {

			c, err := getConfig()
			if err != nil {
				log.Fatal(err)
			}

			lnk := link.Link{Config: c, Url: cCtx.Args().Get(0)}

			err = lnk.Open()
			if err != nil {
				log.Fatal(err)
			}

			return nil

		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func getConfig() (*config.Config, error) {

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("$HOME/.config/opening")

	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			return nil, fmt.Errorf("It looks like you don't have a config file.  You'll need to create one at ${HOME}/.config/opening/config")
		} else {
			return nil, fmt.Errorf("Couldn't read the config file: %w", err)
		}
	}

	var c config.Config

	err = viper.Unmarshal(&c)
	if err != nil {
		return nil, fmt.Errorf("Parsing the config file failed with the error: %w", err)
	}

	return &c, nil

}
