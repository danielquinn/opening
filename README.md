# opening

![opening.png](opening.png)

Open what you want in the browser you want.


## Why this exists

I primarily use Firefox, I prefer to open the occasional thing in other
browsers like Chromium and Epiphany.  So, I just point GNOME at this little
program let it open the link in question in the appropriate browser. 


## How to use it

1. Download the correct `opening` for your machine from the [releases](https://gitlab.com/danielquinn/opening/-/releases)
   page and put it somewhere in your `${PATH}` like `/usr/local/bin`.  Make it
   executable.
2. Create a config file (details below) at `${HOME}/.config/opening/config`
3. Tell your desktop environment (like GNOME, KDE, whatever) to target
   `opening` instead of your preferred browser.
4. Click a link in your email and see what happens :-)


## Configuration

It's just a YAML file with a pretty simple layout.  Here's a sample:

```yaml
# ${HOME}/.config/opening/config 

fallback: firefox

browsers:

  - name: chromium
    command: ["/usr/bin/chromium"]

  - name: epiphany
    command: ["/usr/bin/epiphany"]

  - name: netflix
    command: ["/usr/bin/firefox", "--profile", "Netflix"]

  - name: firefox
    command: ["/usr/bin/firefox"]

urls:

  - regex: ^https://meet.google.com/
    browser: chromium

  - regex: ^https://calendar.google.com/
    browser: chromium

  - regex: ^https://(www\.)?nebula.tv/
    browser: epiphany

  - regex: ^https://(www\.)netflix.com/
    browser: netflix
```

There's just three sections:


### `fallback`

This is the name of the browser you want to use by default.


### `browsers`

A list of browsers including a `name` (we reference this elsewhere in the
file) and the `command` used to start it on your machine.  Note that `command`
here is a list, as you may want to launch a browser with specific arguments.


### `urls`

This is probably the most complicated part.  It's a list of URL/browser
pairings where we use a regular expression to define the URL and the `name`
of the browser mentioned above.  If you're not familiar with regular
expressions, you can just put an explicit URL here instead.  Having the
option of using a regex is just nice for people who know how to use them.